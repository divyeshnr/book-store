# Book Store

A platform to rent books.

The project has folowing directories 

bookstore- The main application which contains the source code.

tests - Test cases for the application

DockerFile - Configuration for building docker image and attach the app in it.

Release.sh - A file which will update the heroku server about the latest built docker image during the CI/CD process.

gitlab-ci.yml - YAML file which carries the configuration for CI/CD of this project.