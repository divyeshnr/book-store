FROM python:3-alpine

MAINTAINER Divyesh

COPY book_store /book_store

WORKDIR /book_store

RUN pip install -r requirements.txt

CMD ["python","server.py"]