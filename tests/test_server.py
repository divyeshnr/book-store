import requests
import json
from datetime import date,datetime,timedelta
import pytest

#Story 3 varying price.
def get_type_price(params,delta):
    total_price=0

    #Get the price for regular on the basis of the issue and return dates
    if params['book_type']=="Regular":
        if delta < 2:
            total_price = params['quantity'] * delta * 2
            params['price'] = 2
        else:
             total_price = (params['quantity'] * delta * 1.5) - \
                            (1 *params['quantity'])
    
    #Get the price for novel on the basis of the issue and return dates         
    if params['book_type']=="Novel":
        if delta < 3:
            total_price = params['quantity'] * delta * 4.5
            params['price'] = 4.5
        else:
             total_price = (params['quantity'] * delta * 1.5) 
    
    #Get the price for fiction on the basis of the issue and return dates
    if params['book_type']=="Fiction":
        total_price = (params['quantity'] * delta * 3)

    return(total_price)    


# Story 3 price calculator
def price_calculator(params,start_date):
    delta = int((params['return_date']-start_date).days)
    if delta == 0:
        return(get_type_price(params,1))
    else:    
        return(get_type_price(params,delta))

#Story 2 testing, optimised and fixed few bugs.
@pytest.mark.skip(reason="Not testing this now since story 2 testing is going on")
def test_place_order():
	url="http://127.0.0.1:2000/placeOrder"
	assert_strings = [
						b'"Please enter a valid name with atleast 1 valid alphabet in the field"',
						b'"Sorry couldn\'t place the order"\n',
						b"Couldn't place the order due to unavailability of price"

						]
	p_data= [
				{'book_name':'','book_type':'','return_date':'','party_name':'','quantity':0,'price':''},
				{'book_name':'abc','book_type':'','return_date':'','party_name':'','quantity':0,'price':''},
				{'book_name':'abc','book_type':'regular','return_date':'2019-09-11','party_name':'Ram','quantity':2.5,'price':'6'},
				{'book_name':'abc','book_type':'regular','return_date':'2019-09-11','party_name':'2','quantity':'2.5','price':'6'},
				{'book_name':'abc','book_type':'regular','return_date':'2019-09-11','party_name':'2ram','quantity':'2.5','price':'6'},
				{'book_name':'abc','book_type':'regular','return_date':'2019-09-11','party_name':'2ram','quantity':'2','price':'Not Available'}
				
			]
	payload = json.dumps(p_data[5]) 
	resp = requests.post(url,data=payload)
	assert  assert_strings[0]== resp.content

@pytest.mark.skip(reason="Not testing this now since story 2 testing is going on")
#Test to find the price if issue date and return date are same then price will be 0.	
def test_price():
	start_date = date.today()
	return_date = datetime.strptime("2019-11-24","%Y-%m-%d").date()

	data = price_calculator(2,1,start_date,return_date)    

	assert data == 0

@pytest.mark.skip(reason="Not testing this now since story 2 testing is going on")
def test_price_back_dated():
	start_date = date.today()
	return_date = datetime.strptime("2019-11-24","%Y-%m-%d").date() - timedelta(1)

	data = price_calculator(2,1,start_date,return_date)    

	assert data == "failed"


def test_price_story3():

	payload = {'book_name':'abc','book_type':'regular','return_date':'2019-09-11','party_name':'2ram','quantity':'2','price':'Not Available'}
	start_date = date.today()
	data = price_calculator
	assert data == 32.5