#!/usr/bin/env python3
from flask import Flask, render_template,request,url_for,redirect,jsonify
import shutil
import json
import os
import sqlite3
from flask_cors import CORS
import yaml
from datetime import date,datetime
import logging

app=Flask(__name__)
CORS(app)
config_data={}


def get_type_price(params,delta):
    total_price=0

    #Get the price for regular on the basis of the issue and return dates
    if params['book_type']=="Regular":
        if delta < int(config_data['regular']['min_d']):
            total_price = params['quantity'] * delta * config_data['regular']['min_c']
            params['price'] = config_data['regular']['min_c']
        else:
             total_price = (params['quantity'] * delta * config_data['regular']['std']) - \
                            (config_data['regular']['prim'] *params['quantity'])
    
    #Get the price for novel on the basis of the issue and return dates         
    if params['book_type']=="Novel":
        if delta < int(config_data['novel']['min_d']):
            total_price = params['quantity'] * delta * config_data['novel']['min_c']
            params['price'] = config_data['novel']['min_c']
        else:
             total_price = (params['quantity'] * delta * config_data['novel']['std']) 
    
    #Get the price for fiction on the basis of the issue and return dates
    if params['book_type']=="Fiction":
        total_price = (params['quantity'] * delta * config_data['fiction']['std'])

    return(total_price)    

#Get the book's price according to the category of book
def get_book_price(book_type):

    if book_type == 'Regular':
        return[config_data['regular']['prim'],config_data['regular']['std'],config_data['regular']['min_d'],
                config_data['regular']['min_c']]
    if book_type == 'Novel':
        return[config_data['novel']['std'],None,config_data['novel']['min_d'],
                config_data['novel']['min_c']]
    if book_type == 'Fiction':
        return[config_data['fiction']['std'],None,None,None]
    else:
        return "Not Available"    


#Calculate the price set by the company.
def price_calculator(params,start_date):
    delta = int((params['return_date']-start_date).days)
    if delta == 0:
        return(get_type_price(params,1))
    else:    
        return(get_type_price(params,delta))

#Params with invalid party name,invalid quantity, back dated return dates neads to validated.
def get_params_validated(order_params,start_date):

    if not any(st.isalpha() for st in order_params['party_name']):
        return("Failed","Please enter a valid name with atleast 1 valid alphabet in the field")

    if order_params['quantity'].isdigit():
        order_params['quantity'] = int(order_params['quantity'])    
    else:
        return("Failed","Please enter a valid quantity")
       
    if order_params['price'] != "Not Available": 
        order_params['price'] = float(order_params['price'])
    else:
        return("Failed","Couldn't place the order due to unavailability of price")

    delta = int((order_params['return_date']-start_date).days)
    if delta < 0:
        return("Failed","Return date can't be less than issued date")
      

    return("Success","All params are valid")


#Api to get the purchase orders of a particular customer
@app.route('/get_invoice',methods=['GET'])
def get_invoice():

    try:
        row_data=[]
        conn = sqlite3.connect("db/bookStore.db")
        cur = conn.cursor()
        party= request.args['party']
        ord_date= datetime.strptime(request.args['order_date'],"%Y-%m-%d").date()
        sql_query = """
                        Select * from
                            transactions
                        where
                            party = '%s' and posting_date ='%s'         
                        """%(party,ord_date)
        cur.execute(sql_query)            
        data = cur.fetchall()
        for row in data:
            row_data.append((row[1],row[2],row[4],row[5],row[6],row[7],row[8]))
        return(json.dumps(row_data))

    except Exception as e:
        logger.error("Function - get_invoice with excpetion %s"%(e))
        

#Invoice page to see all the purchases done by the customer.
@app.route('/history',methods=['GET'])
def history():
    return(render_template('invoice.html',data=""))


#Place order with quantity and return date
@app.route('/placeOrder',methods=['POST'])
def placeOrder():
    order_params = {'book_name':'','book_type':'','party_name':'','quantity':0,'return_date':'','price':0}
    try:
        #Establish connection with the database
        conn = sqlite3.connect("db/bookStore.db")
        cur = conn.cursor()

        #If no parameters are recieved during post request, ask the user to fill the form again.
        if request.data:
            form_params = json.loads(request.data)
            for key,item in order_params.items():
                if key not in form_params or form_params[key]=='':
                    return(jsonify("Sorry couldn't place the order due to missing details"))
                else:
                    order_params[key] = form_params[key]
            start_date = date.today()
            order_params['return_date'] = datetime.strptime(order_params['return_date'],"%Y-%m-%d").date()

            status,msg = get_params_validated(order_params,start_date)
            if status == "Failed":
                return(jsonify(msg))
            else: 
                #Calculate the price from the yml file and formulate it with quantity and days
                total_amount = price_calculator(order_params,start_date)
   
                sql_query = """
                                Insert into transactions
                                    (party,
                                    book,
                                    book_type,
                                    posting_date,
                                    start_date,
                                    end_date,
                                    quantity,
                                    price,
                                    total_amount )
                                values
                                    ('%s','%s','%s','%s','%s','%s','%d','%f','%f')    
                                """%(order_params['party_name'],order_params['book_name'],order_params['book_type'], 
                                    start_date,start_date,order_params['return_date'],order_params['quantity'],
                                    order_params['price'], total_amount)
                cur.execute(sql_query)            
                conn.commit()
                return(jsonify("Order placed successfully"))
        
        else:
            return(jsonify("Sorry couldn't place the order"))    

    except Exception as e:
        logger.error("Function - description with excpetion %s"%(e))
        return(jsonify("Sorry couldn't place the order"))

#Description page to learn the price and place an order
@app.route('/description',methods=['GET'])
def description():

    try:
        price=0
        book_name= request.args['book_name']
        book_type= request.args['book_type'] 
        pid =  request.args['pid']
        price=get_book_price(book_type)
        rows = [book_name,book_type,pid,price] 
        return(render_template('description.html',data=rows))

    except Exception as e:
        logger.error("Function - description with excpetion %s"%(e)) 

#Homepage which will render the books avaliable.
@app.route('/',methods=['GET'])
def homepage():

    conn = sqlite3.connect("db/bookStore.db")
    cur = conn.cursor()
    sql_query = """
                    Select
                        PID,
                        book_name,
                        book_type
                    from 
                        book_collection
                    """
    cur.execute(sql_query)
    rows = list(cur.fetchall())
    return(render_template('product.html',data=rows))


#Get the configuration at the initial stage.    
def get_config(config_file):
    global config
    with open(config_file, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)   
    #config_data['books'] = cfg['books']
    config_data['regular'] = cfg['books']['regular']
    config_data['novel'] = cfg['books']['novel']
    config_data['fiction'] = cfg['books']['fiction']

def log_exceptions():
    logger = logging.getLogger(__name__)  
    logger.setLevel(logging.ERROR)
    file_handler = logging.FileHandler('logs/server.log')
    formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger

#Get the configuration for the and the logger file  
config = get_config('config/config.yml')
logger = log_exceptions()

#The flask app gets the available port
port = int(os.environ.get("PORT", 5000))
app.run(host='0.0.0.0', port=port)




